# report.py
#
# Exercise 2.4
# Exercise 2.5

import csv
from pprint import pprint  # pretty print


def read_portfolio(filename):
    """Return a list of portfolio data dictionaries."""

    portfolio = []

    with open(filename, 'rt') as f:
        lines = csv.reader(f)
        headers = next(lines)

        for line in lines:
            t_dic = {headers[0]: line[0],
                     headers[1]: int(line[1]),
                     headers[2]: float(line[2])}
            portfolio.append(t_dic)

    return portfolio


portfolio = read_portfolio('Data/portfolio.csv')
# pprint(portfolio)  # pretty print
#
# total = 0.0
# for s in portfolio:
#     total += s['shares'] * s['price']
#
# print(total)


# Exercise 2.6

def read_prices(filename):
    """Return a list of dictionary{'stock index': 'price'}."""
    prices = {}

    with open(filename, 'rt') as f:
        lines = csv.reader(f)

        for line in lines:
            try:
                prices[line[0]] = float(line[1])
            except IndexError:
                pass

    return prices


prices = read_prices('Data/prices.csv')
# pprint(prices)
# print(prices['IBM'])
# print(prices['MSFT'])


# Exercise 2.7
#
# total_cost = 0.0
# for s in portfolio:
#     total_cost += s['shares'] * s['price']
#
# print('Total Cost:', total_cost)
#
#
# total_value = 0.0
# for s in portfolio:
#     total_value += s['shares'] * prices[s['name']]
#
# print('Total Value:', total_value)
# print('Gain:', total_value - total_cost)


# Exercise 2.9

def make_report(bought_portfolio: list, current_prices: dict) -> tuple:
    """A tuple of ('name', 'shares', '[current] price', 'change')"""
    result = []
    for s in bought_portfolio:
        t_list = [s['name'],
                  s['shares'],
                  current_prices[s['name']],
                  current_prices[s['name']] - s['price']]
        result.append(tuple(t_list))
    return tuple(result)


report = make_report(portfolio, prices)


# Exercise 2.10
# Exercise 2.11

headers = ('Name', 'Shares', 'Price', 'Change')
print(f'{headers[0]:>10} {headers[1]:>10} {headers[2]:>10} {headers[3]:>10}')
print('-' * 10, '-' * 10, '-' * 10, '-' * 10)
for name, shares, price, change in report:
    print(f'{name:>10} {shares:>10d} {price:>10.2f} {change:>10.2f}')
