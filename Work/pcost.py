# pcost.py
#
# Exercise 1.27
# Exercise 1.30
# Exercise 1.33
# Exercise 2.15
# Exercise 2.16

import sys


def portfolio_cost(filename: str) -> float:
    total_cost = 0.0

    import csv

    with open(f'{filename}', 'rt') as f:
        rows = csv.reader(f)
        headers = next(rows)

        for i, line in enumerate(f, start=1):
            record = dict(zip(headers, line.strip().split(',')))
            try:
                # subject = line.strip().split(',')
                # subject[1] = int(subject[1])
                # subject[2] = float(subject[2])
                # total_cost += subject[1] * subject[2]
                shares = int(record['shares'])
                price = float(record['price'])
                total_cost += shares * price
            except ValueError:
                print(f'Row {i} could not convert: {line}')

    return total_cost


if len(sys.argv) == 2:
    filename = sys.argv[1]
else:
    filename = 'Data/portfolio.csv'

cost = portfolio_cost('Data/portfoliodate.csv')
print('Total cost:', cost)
