# bounce.py
#
# Exercise 1.5

height = 100.0
ratio = 3 / 5

t = 1

while t <= 10:
    height *= ratio
    print(t, round(height, 4))
    t += 1
