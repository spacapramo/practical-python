# mortgage.py
#
# Exercise 1.7
# Exercise 1.8
# Exercise 1.9
# Exercise 1.10
# Exercise 1.11
# Exercise 1.17

principal = 500_000.0
rate = 0.05
payment = 2_684.11
total_paid = 0.0

month = 0

extra_payment_start_month = 61
extra_payment_end_month = 108
extra_payment = 1_000


while principal > 0:
    month += 1
    if extra_payment_start_month <= month <= extra_payment_end_month:
        if principal < (payment + extra_payment):
            payment = principal
            principal -= payment
            total_paid += payment
        else:
            principal = principal * (1+rate/12) - (payment + extra_payment)
            total_paid += (payment + extra_payment)
    else:
        if principal < payment:
            payment = principal
            principal -= payment
            total_paid += payment
        else:
            principal = principal * (1+rate/12) - payment
            total_paid += payment

    print(f'{month:d}, {total_paid:.2f}, {principal:.2f}')


print(f'Total paid {total_paid:.2f}')
print(f'Month {month}')
